import './sass/style.scss'

function App() {
  return (
    <div className="app">
      <div className="header">
        <h1>Weekly Planning</h1>
        <span id="line" ></span>

        <div className="month-year">
          <h3>
            Month: <span>Dicembre</span>
          </h3>

          <h3>
            Year: <span>2022</span>
          </h3>
        </div>
      </div>

      <div className="planner">
        <div id="week">
          <div className="weekday">
            <h3>LUN</h3>

            <span>6</span>
          </div>
        </div>

        <form id="notes">
          <h2>Note:</h2>

          {/* note input */}
          <textarea name="" id="" cols="30" rows="10" placeholder='Write your text && press ENTER to confirm' ></textarea>
          {/* note input */}

          {/* note injection */}
          <div className='enternotes'>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate voluptas, modi ratione ipsum eius aspernatur minus consequatur, at dignissimos porro natus blanditiis odit. Quidem pariatur consequatur rem repudiandae et quisquam.

            <div className="icons">
              <input type="button" id='edit' />
              <input type="button" id='delete' />
            </div>
          </div>
          {/* note injection */}

        </form>

        <form id="todo">
          <h2>Tasks:</h2>

          <div className="tasks">
            <div className="task">

            {/* task input */}
            <div className='task-input'>
                <input type="text" placeholder='What?' name="what-task" id="what-task"/>

                <input placeholder='When?' type="text" name="when-task" id="when-task"/>
            </div>
            {/* task input */}

            {/* task injection */}
            <ul className='task-injection' >
              <li>
                Lorem Ipsum 

                <span id="when">11:30am</span>
              </li>

              <div className="icons">
                <input type="button" id='edit' />
                <input type="button" id='delete' />
              </div>
            </ul>
            {/* task injection */}

            </div>
          </div>

        </form>

      </div>
    </div>
  );
}

export default App;
