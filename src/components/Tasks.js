import React from "react";

function Tasks() {
    return (
        <form id="todo">
          <h2>Tasks:</h2>

          <div className="tasks">
            <div className="task">

            {/* task input */}
            <div className='task-input'>
                <input type="text" placeholder='What?' name="what-task" id="what-task"/>

                <input placeholder='When?' type="text" name="when-task" id="when-task"/>
            </div>
            {/* task input */}

            {/* task injection */}
            <ul className='task-injection' >
              <li>
                Lorem Ipsum 

                <span id="when">11:30am</span>
              </li>

              <div className="icons">
                <input type="button" id='edit' />
                <input type="button" id='delete' />
              </div>
            </ul>
            {/* task injection */}

            </div>
          </div>

        </form>
    )
}

export default Tasks;