import React, {useState} from "react";

function Notes() {
    const [value, setValue] = useState('');

    const enterSubmit = e => {
      e.preventDefault();
      setValue('');
    }

    const takeNotes = e => {
      setValue(e.target.value);
      console.log(value);
    }

    return (
        <form  onSubmit={enterSubmit} id="notes">
          <h2>Note:</h2>

          {/* note input */}
          <input
            placeholder='Write your text && press ENTER to confirm'
            value={value}
            onChange={takeNotes}></input>
          {/* note input */}

          {/* note injection */}
          <div className='enternotes'>
            

            <div className="icons">
              <input type="button" id='edit' />
              <input type="button" id='delete' />
            </div>
          </div>
          {/* note injection */}

        </form>
    );
}

export default Notes;